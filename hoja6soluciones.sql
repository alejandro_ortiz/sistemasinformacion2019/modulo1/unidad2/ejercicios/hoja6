﻿0.USE m1u2h6;

-- 1- Crear una vista que debe mostrar los pedidos de los clientes que son de Madrid, Barcelona o Zaragoza. Visualizar 
-- los campos: CÓDIGO CLIENTE, NUMERO DE PEDIDO, EMPRESA y POBLACIÓN. Llamar a la vista CONSULTA1
  
  -- SIN VISTAS

  -- C1 - Clientes de Madrid, Barcelona o Zaragoza
  SELECT 
    c.`CÓDIGO CLIENTE`, 
    c.EMPRESA, 
    c.POBLACIÓN 
    FROM clientes c 
    WHERE c.POBLACIÓN IN ('Madrid','Barcelona','Zaragoza');

  -- Consulta completa
  SELECT 
    c1.`CÓDIGO CLIENTE`, 
    p.`NÚMERO DE PEDIDO`, 
    c1.EMPRESA, 
    c1.POBLACIÓN 
    FROM pedidos p 
    JOIN (
      SELECT 
        c.`CÓDIGO CLIENTE`, 
        c.EMPRESA, 
        c.POBLACIÓN 
        FROM clientes c 
        WHERE c.POBLACIÓN IN ('Madrid','Barcelona','Zaragoza')
      )c1 
    USING (`CÓDIGO CLIENTE`);

  -- CON VISTAS
  -- Subconsulta1Consulta1Optimizada - Clientes de Madrid, Barcelona o Zaragoza
  CREATE OR REPLACE VIEW Subconsulta1Consulta1Optimizada AS
    SELECT 
      c.`CÓDIGO CLIENTE`, 
      c.EMPRESA, 
      c.POBLACIÓN 
      FROM clientes c 
      WHERE c.POBLACIÓN IN ('Madrid','Barcelona','Zaragoza');

  -- Consulta1Optimizada - Consulta completa
  CREATE OR REPLACE VIEW Consulta1Optimizada AS
    SELECT 
      s.`CÓDIGO CLIENTE`, 
      p.`NÚMERO DE PEDIDO`, 
      s.EMPRESA, 
      s.POBLACIÓN 
      FROM pedidos p 
      JOIN Subconsulta1Consulta1Optimizada s
      USING (`CÓDIGO CLIENTE`);

  -- ejecutar vistas
  SELECT * FROM Consulta1Optimizada co;


-- 2- Crear una vista que nos permita realizar una consulta que muestre que clientes han realizado algún pedido en el
-- año 2002 y su forma de pago fue tarjeta. Utilizamos los campos EMPRESA y POBLACIÓN de la tabla CLIENTES, y
-- NÚMERO DE PEDIDO, FECHA DE PEDIDO y FORMA DE PAGO de la tabla PEDIDOS. Ordenar los registros por el
-- campo FECHA DE PEDIDO de forma ascendente. Guardamos la vista con el nombre CONSULTA 2. La vista debe
-- tener la consulta optimizada. Si necesitas crear más de una vista colócalas el nombre que desees.
  
  -- SIN VISTAS

  -- Pedidos realizados en 2002 con tarjeta
  SELECT p.`NÚMERO DE PEDIDO`,p.`FECHA DE PEDIDO`,p.`FORMA DE PAGO`,p.`CÓDIGO CLIENTE` 
    FROM pedidos p 
    WHERE YEAR(p.`FECHA DE PEDIDO`)=2002 
    AND p.`FORMA DE PAGO`='tarjeta';

  -- consulta final
  SELECT c.EMPRESA, c.POBLACIÓN, c1.`NÚMERO DE PEDIDO`, c1.`FECHA DE PEDIDO`, c1.`FORMA DE PAGO` 
    FROM clientes c 
    JOIN (
      SELECT p.`NÚMERO DE PEDIDO`,p.`FECHA DE PEDIDO`,p.`FORMA DE PAGO`,p.`CÓDIGO CLIENTE` 
        FROM pedidos p 
        WHERE YEAR(p.`FECHA DE PEDIDO`)=2002 
        AND p.`FORMA DE PAGO`='tarjeta'
      )c1 
    USING (`CÓDIGO CLIENTE`) 
    ORDER BY c1.`FECHA DE PEDIDO` ASC;

  -- CON VISTAS
  -- Subconsulta1Consulta2 Pedidos realizados en 2002 con tarjeta
  CREATE OR REPLACE VIEW Subconsulta1Consulta2 AS
    SELECT p.`NÚMERO DE PEDIDO`,p.`FECHA DE PEDIDO`,p.`FORMA DE PAGO`,p.`CÓDIGO CLIENTE` 
      FROM pedidos p 
      WHERE YEAR(p.`FECHA DE PEDIDO`)=2002 
      AND p.`FORMA DE PAGO`='tarjeta';

  -- Consulta2
  CREATE OR REPLACE VIEW Consulta2 AS
    SELECT c.EMPRESA, c.POBLACIÓN, c1.`NÚMERO DE PEDIDO`, c1.`FECHA DE PEDIDO`, c1.`FORMA DE PAGO` 
      FROM clientes c 
      JOIN Subconsulta1Consulta2 c1
      USING (`CÓDIGO CLIENTE`) 
      ORDER BY c1.`FECHA DE PEDIDO` ASC;

  -- ejecutar vistas
  SELECT * FROM Consulta2 c;

-- Crear una vista nueva que nos permita realizar una consulta que nos permita saber los clientes que han realizado
-- más pedidos. Mostrar el código y el teléfono. Guardamos la vista con el nombre de CONSULTA3.

  -- SIN VISTAS

  -- SubconsultaC1 - numero de pedidos de cada cliente
  SELECT COUNT(*)numPedidos, p.`CÓDIGO CLIENTE` 
    FROM pedidos p 
    GROUP BY p.`CÓDIGO CLIENTE`;

  -- SubconsultaC2 - numero maximo de pedidos
  SELECT MAX(c1.numPedidos)maximo 
    FROM (
      SELECT COUNT(*)numPedidos 
        FROM pedidos p 
        GROUP BY p.`CÓDIGO CLIENTE`
      )c1;

  -- SubconsultaC3 - Codigo del cliente que mas pedidos ha realizado
  SELECT c1.`CÓDIGO CLIENTE` 
    FROM (
      SELECT COUNT(*)numPedidos, p.`CÓDIGO CLIENTE` 
        FROM pedidos p 
        GROUP BY p.`CÓDIGO CLIENTE`
      )c1 
    JOIN (
      SELECT MAX(c1.numPedidos)maximo 
        FROM (
        SELECT COUNT(*)numPedidos 
          FROM pedidos p 
          GROUP BY p.`CÓDIGO CLIENTE`
        )c1
      )c2 
    ON c1.numPedidos= c2.maximo;

  -- Consulta final
  SELECT c.`CÓDIGO CLIENTE`, c.TELÉFONO 
    FROM (
      SELECT c1.`CÓDIGO CLIENTE` 
        FROM (
          SELECT COUNT(*)numPedidos, p.`CÓDIGO CLIENTE` 
            FROM pedidos p 
            GROUP BY p.`CÓDIGO CLIENTE`
          )c1 
        JOIN (
          SELECT MAX(c1.numPedidos)maximo 
            FROM (
            SELECT COUNT(*)numPedidos 
              FROM pedidos p 
              GROUP BY p.`CÓDIGO CLIENTE`
            )c1
          )c2 
        ON c1.numPedidos= c2.maximo
      )c3 
      JOIN clientes c
      USING (`CÓDIGO CLIENTE`);

  -- CON VISTAS

  -- c1consulta3 - numero de pedidos de cada cliente
  CREATE OR REPLACE VIEW c1consulta3 AS
    SELECT COUNT(*)numPedidos, p.`CÓDIGO CLIENTE` 
      FROM pedidos p 
      GROUP BY p.`CÓDIGO CLIENTE`;

  -- c2consulta3 - numero maximo de pedidos
  CREATE OR REPLACE VIEW c2consulta3 AS
    SELECT MAX(c1.numPedidos)maximo 
      FROM c1consulta3 c1;

  -- c3consulta3 - Codigo del cliente que mas pedidos ha realizado
  CREATE OR REPLACE VIEW c3consulta3 AS
    SELECT c1.`CÓDIGO CLIENTE` 
      FROM c1consulta3 c1 
      JOIN c2consulta3 c2 
      ON c1.numPedidos= c2.maximo;

  -- Consulta final
  CREATE OR REPLACE VIEW Consulta3 AS
    SELECT c.`CÓDIGO CLIENTE`, c.TELÉFONO 
      FROM c3consulta3 c3 
        JOIN clientes c
        USING (`CÓDIGO CLIENTE`);

  -- ejecutar vistas
  SELECT * FROM Consulta3 c;

